from lru_cache import __version__
from setuptools import setup, find_packages

REQUIRES = []

setup(
    name="lru_cache",
    version=__version__,
    description='A Least Recently Used Cache Implementation',
    author='Ben Ringold',
    author_email='bennydtown@gmail.com',
    url='https://hypoware',
    keywords=['lru', 'cache'],
    packages=find_packages(),
    install_requires=REQUIRES,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2.7',
        'Topic :: Software Development :: Libraries :: Python Modules',
    ],
    long_description='A Least Recently Used Cache Implementation.  '
)
