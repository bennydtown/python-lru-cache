import unittest

from lru_cache import Cache
from robber import expect


class TestCache(unittest.TestCase):

    def test_initialization(self):
        cache = Cache(1)
        expect(cache).to.be.instanceof(Cache)
        expect(cache.max_records).to.eq(1)

    def test_store_item_inempty_cache(self):
        cache = Cache(1)
        cache.store('key0', 'data 0')
        expect(len(cache.records)).to.eq(1)
        expect(len(cache.time_index.index)).to.eq(1)

    def test_store_item_in_non_full_cache(self):
        cache = Cache(2)
        cache.store('key0', 'data 0')
        cache.store('key1', 'data 1')
        expect(len(cache.records)).to.eq(2)
        expect(len(cache.time_index.index)).to.eq(2)

    def test_store_item_in_filled_cache(self):
        cache = Cache(1)
        cache.store('key0', 'data 0')
        cache.store('key1', 'data 1')
        expect(len(cache.records)).to.eq(1)
        expect(len(cache.time_index.index)).to.eq(1)

    def test_store_item_with_existing_key(self):
        cache = Cache(1)
        cache.store('key0', 'data 0')
        cache.store('key0', 'data 0 updated')
        expect(cache.records['key0']).to.eq('data 0 updated')

    def test_get_item_hit(self):
        cache = Cache(1)
        cache.store('key0', 'data 0')
        expect(cache.get('key0')).to.eq('data 0')

    def test_get_item_miss(self):
        cache = Cache(1)
        cache.store('key0', 'data 0')
        expect(cache.get('key1')).to.eq(None)

    def test_get_item_last_access_date(self):
        cache = Cache(3)
        for i in range(3):
            cache.store('key%d' % i, 'data %d' % i)
        expect(cache.time_index.youngest.key).to.eq('key2')
        cache.get('key1')
        expect(cache.time_index.youngest.key).to.eq('key1')

    def test_overwrite_item_last_access_date(self):
        cache = Cache(3)
        for i in range(3):
            cache.store('key%d' % i, 'data %d' % i)
        expect(cache.time_index.youngest.key).to.eq('key2')
        cache.store('key1', 'data 1 updated')
        expect(cache.time_index.youngest.key).to.eq('key1')
