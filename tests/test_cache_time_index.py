import unittest

from lru_cache.cache_time_index import CacheTimeIndex
from robber import expect
from util import expect_to_raise


class TestCacheTimeIndex(unittest.TestCase):

    def test_initialization(self):
        time_index = CacheTimeIndex()
        expect(time_index).to.be.instanceof(CacheTimeIndex)
        expect(time_index.youngest).to.eq(None)

    def test_insert_into_empty_index(self):
        time_index = CacheTimeIndex()
        time_index.insert('key0')
        expect(time_index.index['key0'].key).to.eq('key0')
        expect(time_index.youngest.key).to.eq('key0')
        expect(time_index.oldest.key).to.eq('key0')

    def test_insert_into_non_empty_index(self):
        time_index = CacheTimeIndex()
        time_index.insert('key0')
        time_index.insert('key1')
        expect(time_index.index['key0'].key).to.eq('key0')
        expect(time_index.youngest.key).to.eq('key1')
        expect(time_index.oldest.key).to.eq('key0')

    def test_remove_oldest_from_one_element_index(self):
        time_index = CacheTimeIndex()
        time_index.insert('key0')
        removed_key = time_index.remove_oldest()
        expect(removed_key).to.eq('key0')
        expect(len(time_index.index)).to.eq(0)
        expect(time_index.youngest).to.eq(None)
        expect(time_index.oldest).to.eq(None)

    def test_remove_oldest_from_two_element_index(self):
        time_index = CacheTimeIndex()
        time_index.insert('key0')
        time_index.insert('key1')
        removed_key = time_index.remove_oldest()
        expect(removed_key).to.eq('key0')
        expect(len(time_index.index)).to.eq(1)
        expect(time_index.index['key1'].key).to.eq('key1')
        expect(time_index.youngest.key).to.eq('key1')
        expect(time_index.oldest.key).to.eq('key1')

    def test_touch_existing_key_for_one_node_index(self):
        time_index = CacheTimeIndex()
        time_index.insert('key0')
        time_index.touch_existing_key('key0')
        expect(time_index.youngest.key).to.eq('key0')
        expect(time_index.oldest.key).to.eq('key0')

    def test_touch_youngest_key(self):
        """ Should not change index order """
        time_index = CacheTimeIndex()
        for i in range(3):
            time_index.insert('key%d' % i)
        time_index.touch_existing_key('key2')
        expect(time_index.youngest.key).to.eq('key2')
        expect(time_index.youngest.next_older.key).to.eq('key1')
        expect(time_index.youngest.next_older.next_older.key).to.eq('key0')
        expect(time_index.youngest.next_older.next_older.next_older).to.eq(None)
        expect(time_index.oldest.key).to.eq('key0')
        expect(time_index.oldest.next_younger.key).to.eq('key1')
        expect(time_index.oldest.next_younger.next_younger.key).to.eq('key2')
        expect(time_index.oldest.next_younger.next_younger.next_younger).to.eq(None)

    def test_touch_middle_key(self):
        time_index = CacheTimeIndex()
        for i in range(3):
            time_index.insert('key%d' % i)
        time_index.touch_existing_key('key1')
        expect(time_index.youngest.key).to.eq('key1')
        expect(time_index.youngest.next_older.key).to.eq('key2')
        expect(time_index.youngest.next_older.next_older.key).to.eq('key0')
        expect(time_index.youngest.next_older.next_older.next_older).to.eq(None)
        expect(time_index.oldest.key).to.eq('key0')
        expect(time_index.oldest.next_younger.key).to.eq('key2')
        expect(time_index.oldest.next_younger.next_younger.key).to.eq('key1')
        expect(time_index.oldest.next_younger.next_younger.next_younger).to.eq(None)

    def test_touch_oldest_key(self):
        time_index = CacheTimeIndex()
        for i in range(3):
            time_index.insert('key%d' % i)
        time_index.touch_existing_key('key0')
        expect(time_index.youngest.key).to.eq('key0')
        expect(time_index.youngest.next_older.key).to.eq('key2')
        expect(time_index.youngest.next_older.next_older.key).to.eq('key1')
        expect(time_index.youngest.next_older.next_older.next_older).to.eq(None)
        expect(time_index.oldest.key).to.eq('key1')
        expect(time_index.oldest.next_younger.key).to.eq('key2')
        expect(time_index.oldest.next_younger.next_younger.key).to.eq('key0')
        expect(time_index.oldest.next_younger.next_younger.next_younger).to.eq(None)
