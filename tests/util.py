
class ExpectedException(Exception):
    pass


class expect_to_raise(object):
    def __init__(self, exception):
        self.exception = exception

    def __call__(self, fn):
        def test_decorated(*args, **kwargs):
            try:
                fn(self)
            except self.exception:
                pass
            else:
                raise ExpectedException('Expected to raise, but it did not')

        return test_decorated
