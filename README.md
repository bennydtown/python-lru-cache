# LRU Cache - A Last Recently Used Cache


## Installation

See Setup Below

You need to have the setuptools module installed. You can read the setuptools
documentation here: http://pypi.python.org/pypi/setuptools


## Usage

```python

>>> from lru_cache import Cache
>>>
>>> c = Cache(3)
>>> c.store('Kirk','Captain')
>>> c.get('Kirk')
'Captain'
>>>
>>> # Filling the Cache pushes off the oldest element
...
>>> c.store('Spock','Science Officer')
>>> c.store('Bones','Doctor')
>>> c.store('Sulu','Helmsman')
>>> c.get('Kirk')
>>>
>>> # Getting an existing item keeps the key fresh
...
>>> c.store('Spock')
>>> c.get('Spock')
'Science Officer'
>>> c.store('Uhuru','Communications Officer')
>>> c.get('Spock')
'Science Officer'
>>>
>>> # Overwriting an existing item also keeps the key fresh
...
>>> c.store('Bones','Cranky Guy')
>>> c.store('Chekov','Navigator')
>>> c.get('Bones')
'Cranky Guy'

```


## Code quality/conventions checker

This library is using `flake8`. In order to verify the source for any issues,
type:

```
$ cd /path/to/this/repo
$ flake8 .
```

## Tests

### Setup

```
$ sudo python setup.py develop
$ pip install -r tests/requirements.txt
```

### Running the tests

```
$ nosetests tests/
```

### Test coverage

```
$ nosetests tests/ --with-coverage --cover-package=lru_cache
```

HTML coverage:

```
$ nosetests tests/ --with-coverage --cover-package=lru_cache --cover-html
```
