# An LRU (Least Recently Used) cache.  This is a cache with fixed size
# in terms of the number of items it holds (supplied at instantiation).
# The cache must allow client code to get items from the cache and store
# items to the cache.  Once the cache is full, when the client wants to
# store a new item in the cache, an old item must be overwritten or
# removed to make room. The item we will remove is the Least Recently
# Used (LRU) item.

from cache_time_index import CacheTimeIndex


class Cache(object):
    """
    A Least Recently Used Cache
    """

    def __init__(self, max_records):
        self.max_records = max_records
        self.records = {}
        self.time_index = CacheTimeIndex()

    def store(self, key, data):
        """ Insert data into cache with key """
        self.records[key] = data
        if key in self.time_index.index:
            ## This is an existing key so make this the most recently used key
            self.time_index.touch_existing_key(key)
            return
        if len(self.records) <= self.max_records:
            ## The Cache still has empty slots just insert the new record
            self.time_index.insert(key)
            return
        ## Full Cache and a new key, so insert the new
        ## record and pop the oldest record off the cache
        self.time_index.insert(key)
        old_key = self.time_index.remove_oldest()
        self.records.pop(old_key)

    def get(self, key):
        """ Returns previously stored data with the key or None if key miss """
        if key not in self.records:
            return None
        self.time_index.touch_existing_key(key)
        return self.records[key]
