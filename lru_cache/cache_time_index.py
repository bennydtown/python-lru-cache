# The CacheTimeIndex is a data structure used
# For tracking the order in which a collection of
# keys has been most recently accessed.
#
# The current implementation is a doubly linked list
# with an additional index dictionary to make refreshing
# an existing key a constant time operation.


class CacheTimeIndexNode(object):
    """
    A single node,  storing a key in the index.
    Implemented as a doubly linked list node.
    """

    def __init__(self,  key,  next_older=None,  next_younger=None):
        self.key = key
        self.next_older = next_older
        self.next_younger = next_younger


class CacheTimeIndex(object):
    """
    A data structure for tracking the most recently
    accessed keys in a collection.

    These public function interfaces should remain unchanged regardless
    of the underlying implementation.
    """

    def __init__(self):
        self.oldest = None
        self.youngest = None
        self.index = {}     # Included to make touch_existing_key
                            # a constant time function

    def insert(self, key):
        """
        Insert a node with a new key into the head of the index.
        Consumer is responsible for ensuring this is not a duplicate key
        """
        self.youngest = CacheTimeIndexNode(
            key,  self.youngest
        )
        if not self.youngest.next_older:
            self.oldest = self.youngest
        else:
            assert isinstance(self.youngest.next_older,  CacheTimeIndexNode)
            self.youngest.next_older.next_younger = self.youngest
        self.index[key] = self.youngest

    def remove_oldest(self):
        """
        Remove the oldest node in the index.
        Returns the removed key
        """
        assert isinstance(self.oldest,  CacheTimeIndexNode)
        oldest_key = self.oldest.key
        self.index.pop(oldest_key)
        if self.youngest.key == oldest_key:
            self.youngest = None
        self.oldest = self.oldest.next_younger
        if self.oldest is not None:
            self.oldest.next_older = None
        return oldest_key

    def touch_existing_key(self, key):
        """
        Make move specified key to the head so that it is now the
        youngest (Most recently Used) key.
        Consumer is responsible for ensuring this key exists.
        """
        if key == self.youngest.key:
            return
        target_node = self.index[key]
        assert isinstance(target_node,  CacheTimeIndexNode)
        if self.oldest.key == target_node.key and target_node.next_younger:
            self.oldest = target_node.next_younger
        if target_node.next_younger:
            target_node.next_younger.next_older = target_node.next_older
        if target_node.next_older:
            target_node.next_older.next_younger = target_node.next_younger
        self.youngest.next_younger = target_node
        target_node.next_older = self.youngest
        target_node.next_younger = None
        self.youngest = target_node
